package ro.ansoft.favouritemovies;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import android.util.Log;

public class IMDbHelper {

	private static IMDbHelper INSTANCE;
	
	private IMDbHelper() {
		// singleton
	}
	
	public static IMDbHelper getInstance() {
		if(INSTANCE == null) {
			INSTANCE = new IMDbHelper();
		}
		return INSTANCE;
	}

	public ArrayList<Movie> getMoviesBasedOnSearch(String searchCriteria) {
		ArrayList<Movie> moviesList = new ArrayList<Movie>();
		String html = "";
		searchCriteria = searchCriteria.replace(" ", "+");
		html = excuteTitleSearchPost("http://www.imdb.com/find?ref_=nv_sr_fn&q=" + searchCriteria +"&s=all");
		html = "<html><table>" + html + "</html>";
		moviesList = getMoviesFromHTML(html);
		
		return moviesList;
	}

	private ArrayList<Movie> getMoviesFromHTML(String html) {
		ArrayList<Movie> moviesList = new ArrayList<Movie>();
		Document document = Jsoup.parse(html);

		Elements rows = document.getElementsByTag("tr");
		for(Element row : rows) {
			Elements tdsPhoto = row.getElementsByTag("td").addClass("primary_photo");
			Elements anchorsPhoto = tdsPhoto.get(0).getElementsByTag("a");
			String movieURL = anchorsPhoto.get(0).attr("href");
			movieURL = "http://www.imdb.com" + movieURL;
			Elements imgsPhoto = anchorsPhoto.get(0).getElementsByTag("img");
			String imgSrc = "";
			if(imgsPhoto.size() > 0) {
				imgSrc = imgsPhoto.get(0).attr("src");
			}

			Elements tdsTitle = row.getElementsByTag("td").addClass("result_text");
			Elements anchorTitle = tdsTitle.get(1).getElementsByTag("a");
			String descr = tdsTitle.get(1).ownText();
			String title = anchorTitle.get(0).ownText();

			Movie movie = new Movie();
			movie.setPhotoUrl(imgSrc);
			
			String fullTitle = title + " " +  descr;
			if(fullTitle.endsWith("aka")) {
				fullTitle = fullTitle.substring(0, fullTitle.length() - 3);
				fullTitle = fullTitle.trim();
			}
			
			movie.setTitle(fullTitle);
			movie.setImdbUrl(movieURL);
			moviesList.add(movie);
			//Log.d(TAG, movie.toString());
		}
		return moviesList;
	}

	private String excuteTitleSearchPost(String url) {
		String htmlTitlesHeader = "<tr class=\"findResult odd\">";
		StringBuilder stringBuilder = new StringBuilder();
		try {
			URL imdbSearch = new URL(url);
			BufferedReader in = new BufferedReader(new InputStreamReader(imdbSearch.openStream()));

			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				if(inputLine.startsWith(htmlTitlesHeader)) {
					stringBuilder.append(inputLine);
					break;
				}
			}
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return stringBuilder.toString();
	}

	public String getReleaseDate(String imdbURL) {
		String releaseDate = "";
		String movieReleaseDateHTML = "See all release dates";
		String tvShowReleaseDateHTML = "class=\"segment-link \" > On TV";
		try {
			URL imdbSearch = new URL(imdbURL);
			BufferedReader in = new BufferedReader(new InputStreamReader(imdbSearch.openStream()));

			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				if(inputLine.contains(movieReleaseDateHTML)) {
					if(FavouriteMovieConstants.DBG) Log.d(FavouriteMovieConstants.TAG, "movie: " + inputLine);
					releaseDate = inputLine.substring(inputLine.indexOf(" > ") + 3, inputLine.indexOf("<meta"));
					break;
				} else if(inputLine.contains(tvShowReleaseDateHTML)) {
					if(FavouriteMovieConstants.DBG) Log.d(FavouriteMovieConstants.TAG, "tvShow: " + inputLine);
					in.readLine();
					in.readLine();
					in.readLine();
					in.readLine();
					releaseDate = in.readLine();
					break;
				}
			}
			in.close();
		} catch (IOException e) {
			if(FavouriteMovieConstants.DBG) Log.e(FavouriteMovieConstants.TAG, "get release date exception", e);
		}
		if(FavouriteMovieConstants.DBG) Log.d(FavouriteMovieConstants.TAG, "releaseDate: " + releaseDate);
		return releaseDate.trim();
	}

	public String getIMDBRating(String imdbURL) {
		String rating = "";
		String movieRatingHTML = "titlePageSprite star-box-giga-star";
		try {
			URL imdbSearch = new URL(imdbURL);
			BufferedReader in = new BufferedReader(new InputStreamReader(imdbSearch.openStream()));

			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				if(inputLine.contains(movieRatingHTML)) {
					if(FavouriteMovieConstants.DBG) Log.d(FavouriteMovieConstants.TAG, "movie: " + inputLine);
					rating = inputLine.substring(inputLine.indexOf("\"> ") + 3, inputLine.indexOf(" </div>"));
					break;
				}
			}
			in.close();
		} catch (IOException e) {
			if(FavouriteMovieConstants.DBG) Log.e(FavouriteMovieConstants.TAG, "get rating exception", e);
		}
		if(FavouriteMovieConstants.DBG) Log.d(FavouriteMovieConstants.TAG, "rating: " + rating);
		return rating.trim();
	}

	public String getIMDBDescription(String imdbURL) {
		String description = "";
		String descriptionHTML = "itemprop=\"description\">";
		try {
			URL imdbSearch = new URL(imdbURL);
			BufferedReader in = new BufferedReader(new InputStreamReader(imdbSearch.openStream()));

			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				if(inputLine.contains(descriptionHTML)) {
					if(FavouriteMovieConstants.DBG) Log.d(FavouriteMovieConstants.TAG, "description line: " + inputLine);
					if(!inputLine.contains("</p>")) {
						String tempLine = "";
						do {
							tempLine = in.readLine();
							inputLine += tempLine;
						} while(!tempLine.contains("</p>"));
					}
					if(FavouriteMovieConstants.DBG) Log.d(FavouriteMovieConstants.TAG, "description: " + inputLine);
					description = inputLine.substring(descriptionHTML.length() + 3, inputLine.indexOf("</p>"));
					break;
				}
			}
			in.close();
		} catch (IOException e) {
			if(FavouriteMovieConstants.DBG) Log.e(FavouriteMovieConstants.TAG, "get description exception", e);
		}
		if(FavouriteMovieConstants.DBG) Log.d(FavouriteMovieConstants.TAG, "description: " + description);
		return description.trim();
	}

}
