package ro.ansoft.favouritemovies;

import java.util.ArrayList;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class FavouriteMoviesPagerAdapter extends FragmentPagerAdapter {

	private ArrayList<Fragment> fragmentsList;

	public FavouriteMoviesPagerAdapter(FragmentManager fragmentManager) {
	    super(fragmentManager);
    }

	public void setFragmentList(ArrayList<Fragment> fragmentsList) {
		this.fragmentsList = fragmentsList;
	}

	@Override
	public int getCount() {
	    return fragmentsList.size();
	}

	@Override
	public Fragment getItem(int arg0) {
	    return fragmentsList.get(arg0);
	}

}
