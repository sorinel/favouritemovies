package ro.ansoft.favouritemovies;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class BootCompletedReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.d(FavouriteMovieConstants.TAG, "Boot Completed");
		context.startService(new Intent(context, CheckNotificationService.class));
	}
	
}
