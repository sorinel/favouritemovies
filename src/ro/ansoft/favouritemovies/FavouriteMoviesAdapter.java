package ro.ansoft.favouritemovies;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class FavouriteMoviesAdapter  extends ArrayAdapter<Movie> {

	private Context mContext;
	private ArrayList<Movie> mMoviesList;
	private ViewHolder viewHolder;

	public FavouriteMoviesAdapter(Context context, int resource, ArrayList<Movie> movies) {
	    super(context, resource, movies);
	    mContext = context;
	    mMoviesList = movies;
    }

	@Override
	public int getCount() {
	    return mMoviesList.size();
	}

	@Override
	public Movie getItem(int position) {
	    return mMoviesList.get(position);
	}

	@Override
	public int getPosition(Movie item) {
	    return mMoviesList.indexOf(item);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {		
	     if(convertView == null) {
	        LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
	        convertView = inflater.inflate(R.layout.movie_row, parent, false);

	        viewHolder = new ViewHolder();
	        viewHolder.movieImage = (ImageView) convertView.findViewById(R.id.movie_image);
	        viewHolder.movieTitle = (TextView) convertView.findViewById(R.id.movie_title);
	        viewHolder.movieReleaseDate = (TextView) convertView.findViewById(R.id.movie_release_date);

	        convertView.setTag(viewHolder);
	    } else {
	        viewHolder = (ViewHolder) convertView.getTag();
	    }

	    // object item based on the position
	    Movie movie = mMoviesList.get(position);

	    // assign values if the object is not null
	    if(movie != null) {
	    	viewHolder.movieTitle.setText(movie.getTitle());
	    	viewHolder.movieReleaseDate.setText(movie.getReleaseDate());
	    	viewHolder.imageURL = movie.getPhotoUrl();
	    	new DownloadImageTask().execute(viewHolder);
	    	//new DownloadImageTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, viewHolder);
	    }

	    return convertView;
	}

	static class ViewHolder {
		ImageView movieImage;
		TextView movieTitle;
		TextView movieReleaseDate;
		Bitmap imageBitmap;
		String imageURL;
	}

}
