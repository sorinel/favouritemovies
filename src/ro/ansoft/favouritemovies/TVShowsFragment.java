package ro.ansoft.favouritemovies;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class TVShowsFragment extends Fragment {

	private static final String TAG = "FavouriteMoviesActivity";
	private ListView mTVShowsListView;
	private FavouriteMoviesAdapter mTVShowsAdapter;
	private ArrayList<Movie> mTVShowsList;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_tv_shows, container, false);
	    mTVShowsListView = (ListView) rootView.findViewById(R.id.tv_shows_list);

	    mTVShowsList = new ArrayList<Movie>();
	    mTVShowsAdapter = new FavouriteMoviesAdapter(getActivity(), -1, mTVShowsList);
	    mTVShowsListView.setAdapter(mTVShowsAdapter);
	    return rootView;
	}
	
	@Override
	public void onAttach(Activity activity) {
	    super.onAttach(activity);
	    Log.d(TAG, "TVShowsFragment - on attach()");
	}
	
	@Override
	public void onStart() {
	    super.onStart();
	}
	
	@Override
	public void onDetach() {
	    super.onDetach();
	}
	
	@Override
	public void onDestroyView() {
	    super.onDestroyView();
	}
	

}
