package ro.ansoft.favouritemovies;

import android.os.Parcel;
import android.os.Parcelable;

public class Movie implements Parcelable {

	private String id;
	private String title;
	private String releaseDate;
	private String photoUrl;
	private String imdbUrl;
	private String rating;
	private String description;

	public Movie() {
		
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getPhotoUrl() {
		return photoUrl;
	}

	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}

	public String getImdbUrl() {
		return imdbUrl;
	}

	public void setImdbUrl(String imdbUrl) {
		this.imdbUrl = imdbUrl;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return new StringBuilder().append("Movie[title: ").append(this.getTitle()).append(", photoURL: ").append(this.getPhotoUrl())
				.append(", movieURL: ").append(this.getImdbUrl()).append(", releaseDate: ").append(this.getReleaseDate()).toString();
	}

	public Movie(Parcel in){
        String[] data = new String[7];
        in.readStringArray(data);

        this.id = data[0];
        this.title = data[1];
        this.releaseDate = data[2];
        this.photoUrl = data[3];
        this.imdbUrl = data[4];
        this.rating = data[5];
        this.description = data[6];
    }

	@Override
	public int describeContents() {
	    return 0;
	}

	@Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(new String[] {this.id,
                                            this.title,
                                            this.releaseDate,
                                            this.photoUrl,
                                            this.imdbUrl,
                                            this.rating,
                                            this.description});
    }

	public static final Parcelable.Creator<Movie> CREATOR = new Parcelable.Creator<Movie>() {
        @Override
		public Movie createFromParcel(Parcel in) {
            return new Movie(in); 
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };

}
