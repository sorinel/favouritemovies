package ro.ansoft.favouritemovies;

import java.util.ArrayList;

import ro.ansoft.favouritemovies.FavouriteMoviesAdapter.ViewHolder;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class SearchMoviesAdapter  extends ArrayAdapter<Movie> {

	private Context mContext;
	private ArrayList<Movie> mMoviesList;
	
	public SearchMoviesAdapter(Context context, int resource, ArrayList<Movie> movies) {
	    super(context, resource, movies);
	    mContext = context;
	    mMoviesList = movies;
    }
	
	@Override
	public int getCount() {
	    return mMoviesList.size();
	}
	
	@Override
	public Movie getItem(int position) {
	    return mMoviesList.get(position);
	}

	@Override
	public int getPosition(Movie item) {
	    return mMoviesList.indexOf(item);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;
	     if(convertView == null) {
	        LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
	        convertView = inflater.inflate(R.layout.search_movie_row, parent, false);

	        viewHolder = new ViewHolder();
	        viewHolder.movieImage = (ImageView) convertView.findViewById(R.id.search_movie_image);
	        viewHolder.movieTitle = (TextView) convertView.findViewById(R.id.search_movie_title);

	        convertView.setTag(viewHolder);
	    } else {
	        viewHolder = (ViewHolder) convertView.getTag();
	    }

	    Movie movie = mMoviesList.get(position);

	    if(movie != null) {
	    	viewHolder.movieTitle.setText(movie.getTitle());
	    	viewHolder.imageURL = movie.getPhotoUrl();
	    	new DownloadImageTask().execute(viewHolder);
	    }

	    return convertView;
	}
	
}
