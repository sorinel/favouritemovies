package ro.ansoft.favouritemovies;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class FavouriteMoviesHelper {

	private static FavouriteMoviesHelper INSTANCE;
	
	private FavouriteMoviesHelper() {
		
	}
	
	public static FavouriteMoviesHelper getInstance() {
		if(INSTANCE == null) {
			INSTANCE = new FavouriteMoviesHelper();
		}
		return INSTANCE;
	}

	public boolean isOnline(Context context) {
		ConnectivityManager connec = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

		if (connec != null && (connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED) || 
		    (connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED)) { 
		        return true;
		} else if (connec.getNetworkInfo(0).getState() == NetworkInfo.State.DISCONNECTED ||
		         connec.getNetworkInfo(1).getState() == NetworkInfo.State.DISCONNECTED ) {            
				return false;
		}
		return false;
	}
	
}
