package ro.ansoft.favouritemovies;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RatingBar;

public class MoviesFragment extends Fragment {

	private static final String TAG = "FavouriteMoviesActivity";
	private ListView mMoviesListView;
	private FavouriteMoviesAdapter mMoviesAdapter;
	private ArrayList<Movie> mMoviesList;
	private ProgressDialog progressDialog;
	private JSONParser jsonParser;
	private String currentMovieId;
	private String currentMovieReview;
	private String currentMovieRating;
	private SharedPreferences prefs;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	    View rootView = inflater.inflate(R.layout.fragment_movies, container, false);
	    mMoviesListView = (ListView) rootView.findViewById(R.id.movies_list);
	    jsonParser = new JSONParser();
	    registerForContextMenu(mMoviesListView);

	    mMoviesList = new ArrayList<Movie>();
	    mMoviesAdapter = new FavouriteMoviesAdapter(getActivity(), -1, mMoviesList);
	    mMoviesListView.setAdapter(mMoviesAdapter);
	    mMoviesListView.setOnItemClickListener(new OnItemClickListener() {
	    	@Override
	    	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
	    		Movie selectedMovie = mMoviesList.get(position);
	    		ViewMovieDetailsFragment fragmentDetails = new ViewMovieDetailsFragment();

	    		Bundle args = new Bundle();
	    		args.putParcelable(FavouriteMovieConstants.MOVIE, selectedMovie);
	    		fragmentDetails.setArguments(args);

	    		fragmentDetails.show(getActivity().getFragmentManager(), "");
	    		Log.d(TAG, "Selected movie ID: " + selectedMovie.getId());
	    	}
		});
	    prefs = getActivity().getSharedPreferences(FavouriteMovieConstants.PREF_NAME, Context.MODE_PRIVATE);

	    return rootView;
	}

	@Override
	public void onAttach(Activity activity) {
	    super.onAttach(activity);
	    Log.d(TAG, "MoviesFragment - on attach()");
	}

	@Override
	public void onStart() {
	    super.onStart();
	    if(FavouriteMoviesHelper.getInstance().isOnline(getActivity())) {
	    	new LoadMoviesTask().execute();
	    }
	}

	@Override
	public void onDetach() {
	    super.onDetach();
	}

	@Override
	public void onDestroyView() {
	    super.onDestroyView();
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
	    super.onCreateContextMenu(menu, v, menuInfo);
	    MenuInflater inflater = getActivity().getMenuInflater();
	    inflater.inflate(R.menu.movies_options, menu);
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
		int itemId = item.getItemId();
		currentMovieId = mMoviesList.get(info.position).getId();
		switch(itemId) {
		case R.id.action_delete_movie:
			if(FavouriteMoviesHelper.getInstance().isOnline(getActivity())) {
				new DeleteMovieTask().execute(currentMovieId);
			}
			break;
		case R.id.action_add_review:
			showAddReviewDialog();
			break;
		}
	    return super.onContextItemSelected(item);
	}
	
	private void showAddReviewDialog() {
		LayoutInflater inflater = getActivity().getLayoutInflater();
		View dialogView = inflater.inflate(R.layout.add_review, null);
		final EditText reviewEditText = (EditText) dialogView.findViewById(R.id.add_review_text);
		final RatingBar ratingBar = (RatingBar) dialogView.findViewById(R.id.add_review_rating);

		new AlertDialog.Builder(getActivity())
		        .setTitle("Add review")
		        .setView(dialogView)
		        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
			                public void onClick(DialogInterface dialog, int which) {
				                currentMovieReview = reviewEditText.getText().toString();
				                currentMovieRating = "" + ratingBar.getRating();
				                
				                Log.d(FavouriteMovieConstants.TAG, "Current movie review: " + currentMovieReview);
				                Log.d(FavouriteMovieConstants.TAG, "Current movie rating: " + currentMovieRating);
				                new AddReviewTask().execute(new String[]{currentMovieId, currentMovieReview, currentMovieRating});
			                }
		                })
		        .setNegativeButton(android.R.string.no,
		                new DialogInterface.OnClickListener() {
			                public void onClick(DialogInterface dialog, int which) {
				                
			                }
		                })
		        .setIcon(android.R.drawable.ic_menu_edit).show();
	}
	
	private class LoadMoviesTask extends AsyncTask<String, String, String> {
		@Override
		protected void onPreExecute() {
		    super.onPreExecute();
		    progressDialog = new ProgressDialog(getActivity());
		    progressDialog.setMessage(getString(R.string.loading));
		    progressDialog.setCancelable(false);
		    progressDialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			try {
				String userID = prefs.getString(FavouriteMovieConstants.PREF_USER_ID, "");
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
				nameValuePairs.add(new BasicNameValuePair(FavouriteMovieConstants.CATEGORY, "movies"));
				nameValuePairs.add(new BasicNameValuePair(FavouriteMovieConstants.USER_ID, userID));

				JSONObject json = jsonParser.makeHttpRequest(FavouriteMovieConstants.urlGetMoviesList, JSONParser.POST, nameValuePairs);
				saveMovies(json);
				Log.d(FavouriteMovieConstants.TAG, json.toString());
			} catch (Exception e) {
				Log.e(FavouriteMovieConstants.TAG, "JSON Error", e);
			}
		    return null;
		}

		private void saveMovies(JSONObject json) throws JSONException {
			mMoviesList.clear();
			JSONArray jsonIds = json.getJSONArray("ids");
			JSONArray jsonTitles = json.getJSONArray("titles");
			JSONArray jsonReleaseDates = json.getJSONArray("releaseDates");
			JSONArray jsonPhotoURLs = json.getJSONArray("photoURLs");
			JSONArray jsonRatings = json.getJSONArray("ratings");
			JSONArray jsonDescriptions = json.getJSONArray("descriptions");

			for(int i = 0; i < jsonTitles.length(); i++) {
				String id = jsonIds.getString(i);
				String title = jsonTitles.getString(i);
				String releaseDate = jsonReleaseDates.getString(i);
				String photoUrl = jsonPhotoURLs.getString(i);
				String rating = jsonRatings.getString(i);
				String description = jsonDescriptions.getString(i);

				Movie movie = new Movie();
				movie.setId(id);
				movie.setTitle(title);
				movie.setReleaseDate(releaseDate);
				movie.setPhotoUrl(photoUrl);
				movie.setRating(rating);
				movie.setDescription(description);
				mMoviesList.add(movie);
			}
			MoviesFragment.this.getActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					mMoviesAdapter.notifyDataSetChanged();
				}
			});
		}
		
		@Override
		protected void onPostExecute(String result) {
		    super.onPostExecute(result);
		    if(progressDialog != null && progressDialog.isShowing()) {
		    	progressDialog.dismiss();
		    }
		}
	}

	private class DeleteMovieTask extends AsyncTask<String, String, String> {
		@Override
		protected void onPreExecute() {
		    super.onPreExecute();
		    progressDialog = new ProgressDialog(getActivity());
		    progressDialog.setMessage(getString(R.string.deleting_movie));
		    progressDialog.setCancelable(false);
		    progressDialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			if(params == null || params.length != 1) {
				return null;
			}
			try {
				String movieId = params[0];
				String userId = prefs.getString(FavouriteMovieConstants.PREF_USER_ID, "");
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
				nameValuePairs.add(new BasicNameValuePair(FavouriteMovieConstants.CATEGORY, "movies"));
				nameValuePairs.add(new BasicNameValuePair(FavouriteMovieConstants.MOVIE_ID, movieId));
				nameValuePairs.add(new BasicNameValuePair(FavouriteMovieConstants.USER_ID, userId));
				Log.d(FavouriteMovieConstants.TAG, "delete movie http params: " + nameValuePairs.toString());

				JSONObject json = jsonParser.makeHttpRequest(FavouriteMovieConstants.urlDeleteMovieList, JSONParser.POST, nameValuePairs);
				String responseCode = json.getString(FavouriteMovieConstants.JSON_RESPONSE_CODE);
				if(FavouriteMovieConstants.JSON_RESPONSE_OK.equals(responseCode)) {
					
				}
				Log.d(FavouriteMovieConstants.TAG, json.toString());
			} catch (Exception e) {
				Log.e(FavouriteMovieConstants.TAG, "JSON Error", e);
			}
		    return null;
		}

		@Override
		protected void onPostExecute(String result) {
		    super.onPostExecute(result);
		    if(progressDialog != null && progressDialog.isShowing()) {
		    	progressDialog.dismiss();
		    }
		    if(FavouriteMoviesHelper.getInstance().isOnline(getActivity())) {
		    	new LoadMoviesTask().execute();
		    }
		}
	}

	private class AddReviewTask extends AsyncTask<String, String, String> {
		@Override
		protected void onPreExecute() {
		    super.onPreExecute();
		    progressDialog = new ProgressDialog(getActivity());
		    progressDialog.setMessage(getString(R.string.adding_review));
		    progressDialog.setCancelable(false);
		    progressDialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			try {
				String userId = prefs.getString(FavouriteMovieConstants.PREF_USER_ID, "");
				String movieId = params[0];
				String movieReview = params[1];
				String movieRating = params[2];

				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
				nameValuePairs.add(new BasicNameValuePair(FavouriteMovieConstants.MOVIE_ID, movieId));
				nameValuePairs.add(new BasicNameValuePair(FavouriteMovieConstants.USER_ID, userId));
				nameValuePairs.add(new BasicNameValuePair(FavouriteMovieConstants.REVIEW, movieReview));
				nameValuePairs.add(new BasicNameValuePair(FavouriteMovieConstants.RATING, movieRating));

				Log.d(FavouriteMovieConstants.TAG, "add movie review http params: " + nameValuePairs.toString());

				JSONObject json = jsonParser.makeHttpRequest(FavouriteMovieConstants.urlAddReview, JSONParser.POST, nameValuePairs);
				String responseCode = json.getString(FavouriteMovieConstants.JSON_RESPONSE_CODE);
				String responseMessage = json.getString(FavouriteMovieConstants.JSON_RESPONSE_MESSAGE);
				if(FavouriteMovieConstants.JSON_RESPONSE_OK.equals(responseCode)) {
					showDialog(responseMessage);
				}
				Log.d(FavouriteMovieConstants.TAG, json.toString());
			} catch (Exception e) {
				Log.e(FavouriteMovieConstants.TAG, "JSON Error", e);
			}
		    return null;
		}

		@Override
		protected void onPostExecute(String result) {
		    super.onPostExecute(result);
		    if(progressDialog != null && progressDialog.isShowing()) {
		    	progressDialog.dismiss();
		    }
		    if(FavouriteMoviesHelper.getInstance().isOnline(getActivity())) {
		    	new LoadMoviesTask().execute();
		    }
		}
	}

	private void showDialog(final String message) {
		getActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				AlertDialog.Builder alertDialog  = new AlertDialog.Builder(getActivity());
				alertDialog.setTitle(getString(R.string.app_name));
				alertDialog.setMessage(message);
				alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {

					}
				});
				alertDialog.setCancelable(false);
				alertDialog.create().show();
			}
		});
	}

}
