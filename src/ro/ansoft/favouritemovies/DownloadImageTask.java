package ro.ansoft.favouritemovies;

import java.io.IOException;
import java.net.URL;

import ro.ansoft.favouritemovies.FavouriteMoviesAdapter.ViewHolder;

import android.graphics.BitmapFactory;
import android.os.AsyncTask;

public class DownloadImageTask extends AsyncTask<ViewHolder, Void, ViewHolder> {

	@Override
	protected ViewHolder doInBackground(ViewHolder... params) {
		ViewHolder viewHolder = params[0];
		try {
			URL imageURL = new URL(viewHolder.imageURL);
			viewHolder.imageBitmap = BitmapFactory.decodeStream(imageURL.openStream());
		} catch (IOException e) {
			viewHolder.imageBitmap = null;
		}

		return viewHolder;
	}

	@Override
	protected void onPostExecute(ViewHolder result) {
		if (result.imageBitmap == null) {
			result.movieImage.setImageResource(R.drawable.ic_default_movie_image);
		} else {
			result.movieImage.setImageBitmap(result.imageBitmap);
		}
	}
}
