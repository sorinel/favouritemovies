package ro.ansoft.favouritemovies;

import java.util.ArrayList;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;

public class FavouriteMoviesActivity extends FragmentActivity {

	private ViewPager mViewPager;
	private FavouriteMoviesPagerAdapter mPagerAdapter;
	private ActionBar.TabListener tabListener;
	private static final String[] ACTION_BAR_TABS = {"Movies", "TV Shows"};
	private String responseMessage;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mViewPager = (ViewPager) findViewById(R.id.view_pager);

		ArrayList<Fragment> fragments = new ArrayList<Fragment>();
		fragments.add(new MoviesFragment());
		fragments.add(new TVShowsFragment());

		mPagerAdapter = new FavouriteMoviesPagerAdapter(getSupportFragmentManager());
		mPagerAdapter.setFragmentList(fragments);
		mViewPager.setAdapter(mPagerAdapter);
		
		mViewPager.setOnPageChangeListener(
				new ViewPager.SimpleOnPageChangeListener() {
					@Override
					public void onPageSelected(int position) {
						getActionBar().setSelectedNavigationItem(position);
					}
				});

		getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		createTabs();
	}

	private void createTabs() {
			tabListener = new ActionBar.TabListener() {

			@Override
			public void onTabUnselected(Tab tab, FragmentTransaction ft) {

			}

			@Override
			public void onTabSelected(Tab tab, FragmentTransaction ft) {
				mViewPager.setCurrentItem(tab.getPosition());
			}

			@Override
			public void onTabReselected(Tab tab, FragmentTransaction ft) {

			}
		};

		//for(int i = 0; i < ACTION_BAR_TABS.length; i++) {
		for(int i = 0; i < 1; i++) {
			getActionBar().addTab(getActionBar().newTab()
					.setText(ACTION_BAR_TABS[i])
					.setTabListener(tabListener)
					);
		}
	}

	@Override
	protected void onStart() {
	    super.onStart();
	}

	@Override
	protected void onResume() {
	    super.onResume();
	}

	@Override
	protected void onStop() {
	    super.onStop();
	}

	@Override
	protected void onDestroy() {
	    super.onDestroy();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_actionbar, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml
		int id = item.getItemId();
		if (id == R.id.action_add) {
			int currentTab = getActionBar().getSelectedTab().getPosition();
			Intent startAddNewMovieActivity = new Intent(this, AddNewMovieActivity.class);
			startAddNewMovieActivity.putExtra(FavouriteMovieConstants.SELECTED_TAB, currentTab);
			startActivity(startAddNewMovieActivity);
		}
		return super.onOptionsItemSelected(item);
	}

	private void showDialog() {
		AlertDialog.Builder alertDialog  = new AlertDialog.Builder(this);

		alertDialog.setTitle(getString(R.string.app_name));
		alertDialog.setMessage(responseMessage);
		alertDialog.setPositiveButton("OK", new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {

			}
		});
		alertDialog.setCancelable(false);
		alertDialog.create().show();
	}

}
