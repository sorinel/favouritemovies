package ro.ansoft.favouritemovies;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class CreateAccountActivity extends Activity {

	private EditText userNameEditText;
	private EditText emailEditText;
	private EditText passwordEditText;
	private EditText confirmPasswordEditText;
	private Button cancelButon;
	private Button createAccountButton;
	private JSONParser jsonParser;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_create_account);

	    jsonParser = new JSONParser();

	    userNameEditText = (EditText) findViewById(R.id.create_account_user_name);
	    emailEditText = (EditText) findViewById(R.id.create_account_email);
	    passwordEditText = (EditText) findViewById(R.id.create_account_password1);
	    confirmPasswordEditText = (EditText) findViewById(R.id.create_account_password2);
	    cancelButon = (Button) findViewById(R.id.cancel_button);
	    createAccountButton = (Button) findViewById(R.id.create_account_button);

	    cancelButon.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				CreateAccountActivity.this.finish();
			}
		});

	    createAccountButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				validateAccountCreation();
			}
		});
	}

	@Override
	protected void onStart() {
	    super.onStart();
	}

	@Override
	protected void onResume() {
	    super.onResume();
	}

	@Override
	protected void onStop() {
	    super.onStop();
	}

	@Override
	protected void onDestroy() {
	    super.onDestroy();
	}

	private void validateAccountCreation() {
		String userName = userNameEditText.getText().toString();
		String email = emailEditText.getText().toString();
		String password = passwordEditText.getText().toString();
		String confirmPassword = confirmPasswordEditText.getText().toString();

		if(userName == null || userName.isEmpty()) {
			Toast.makeText(this, "User name must not be empty", Toast.LENGTH_SHORT).show();
			return;
		}
		if(email == null || email.isEmpty()) {
			Toast.makeText(this, "Email must not be empty", Toast.LENGTH_SHORT).show();
			return;
		}
		if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
			Toast.makeText(this, "Email must be valid", Toast.LENGTH_SHORT).show();
			return;
		}
		if(password == null || password.isEmpty()) {
			Toast.makeText(this, "Password must not be empty", Toast.LENGTH_SHORT).show();
			return;
		}
		if(confirmPassword == null || confirmPassword.isEmpty()) {
			Toast.makeText(this, "Confirm password must not be empty", Toast.LENGTH_SHORT).show();
			return;
		}
		if(!confirmPassword.equals(password)) {
			Toast.makeText(this, "Passwords do not match", Toast.LENGTH_SHORT).show();
			return;
		}

		if(FavouriteMoviesHelper.getInstance().isOnline(this)) {
			new CreateAccountTask().execute();
		} else {
			Toast.makeText(this, "No internet connection", Toast.LENGTH_SHORT).show();
		}
	}

	private class CreateAccountTask extends AsyncTask<String, String, String> {
		private ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
		    super.onPreExecute();
		    progressDialog = new ProgressDialog(CreateAccountActivity.this);
		    progressDialog.setMessage(getString(R.string.creating_account));
		    progressDialog.setCancelable(false);
		    progressDialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			String userName = userNameEditText.getText().toString();
			String email = emailEditText.getText().toString();
			String password = passwordEditText.getText().toString();

			try {
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
				nameValuePairs.add(new BasicNameValuePair(FavouriteMovieConstants.JSON_USER_NAME, userName));
				nameValuePairs.add(new BasicNameValuePair(FavouriteMovieConstants.JSON_EMAIL, email));
				nameValuePairs.add(new BasicNameValuePair(FavouriteMovieConstants.JSON_PASSWORD, password));
				if(FavouriteMovieConstants.DBG) Log.d(FavouriteMovieConstants.TAG, "create account http params: " + nameValuePairs.toString());

				JSONObject json = jsonParser.makeHttpRequest(FavouriteMovieConstants.urlCreateAccount, JSONParser.POST, nameValuePairs);
				String responseCode = json.getString(FavouriteMovieConstants.JSON_RESPONSE_CODE);
				String message = json.getString(FavouriteMovieConstants.JSON_RESPONSE_MESSAGE);

				if(FavouriteMovieConstants.JSON_RESPONSE_OK.equals(responseCode)) {
					showDialog(message, true);
				} else {
					showDialog(message, false);
				}
				Log.d(FavouriteMovieConstants.TAG, json.toString());
			} catch (Exception e) {
				Log.e(FavouriteMovieConstants.TAG, "JSON Error", e);
			}
		    return null;
		}

		@Override
		protected void onPostExecute(String result) {
		    super.onPostExecute(result);
		    if(progressDialog != null && progressDialog.isShowing()) {
		    	progressDialog.dismiss();
		    }
		}
	}

	private void showDialog(final String message, final boolean shouldExit) {
		CreateAccountActivity.this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				AlertDialog.Builder alertDialog  = new AlertDialog.Builder(CreateAccountActivity.this);

				alertDialog.setTitle(getString(R.string.app_name));
				alertDialog.setMessage(message);
				alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if(shouldExit) {
							CreateAccountActivity.this.finish();
						}
					}
				});
				alertDialog.setCancelable(false);
				alertDialog.create().show();
			}
		});
	}

}
