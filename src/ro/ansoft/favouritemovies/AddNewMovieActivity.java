package ro.ansoft.favouritemovies;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class AddNewMovieActivity extends Activity {

	private EditText mMediaNameEditText;
	private ProgressDialog progressDialog;
	private JSONParser jsonParser;
	private String mediaItem;
	private String responseMessage;
	private ArrayList<Movie> mMoviesList;
	private SharedPreferences prefs;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_add_new_movie);

	    mMediaNameEditText = (EditText) findViewById(R.id.add_new_media);
	    int selectedTab = getIntent().getExtras().getInt(FavouriteMovieConstants.SELECTED_TAB);
	    if(selectedTab == 0) {
	    	mediaItem = "movie";
	    }
	    if(selectedTab == 1) {
	    	mediaItem = "tv show";
	    }
	    mMediaNameEditText.setHint(mMediaNameEditText.getHint() + " " + mediaItem);
	    jsonParser = new JSONParser();
	    prefs = getSharedPreferences(FavouriteMovieConstants.PREF_NAME, Context.MODE_PRIVATE);
	}

	@Override
	protected void onStart() {
		super.onStart();
	};

	@Override
	protected void onResume() {
	    super.onResume();
	}

	@Override
	protected void onStop() {
	    super.onStop();
	}

	@Override
	protected void onDestroy() {
	    super.onDestroy();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.add_new_media, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml
		int id = item.getItemId();
		if (id == R.id.action_search) {
			InputMethodManager inputMethodManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
			inputMethodManager.hideSoftInputFromWindow(mMediaNameEditText.getWindowToken(), 0);
			searchMovie();
		}
		return super.onOptionsItemSelected(item);
	}

	private void searchMovie() {
		if(FavouriteMoviesHelper.getInstance().isOnline(this)) {
			if(!mMediaNameEditText.getText().toString().isEmpty()) {
				new SearchMovieTask().execute(mMediaNameEditText.getText().toString());
			}
		} else {
			Toast.makeText(this, "No internet connection", Toast.LENGTH_SHORT).show();
		}
	}

	private void showFoundMoviesDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(AddNewMovieActivity.this);
		View foundMovies = (View) getLayoutInflater().inflate(R.layout.activity_show_found_movies, null);
        builder.setView(foundMovies);
        builder.setTitle(getString(R.string.choose_title));
        final ListView listView = (ListView) foundMovies.findViewById(R.id.found_movies_list);
        listView.setAdapter(new SearchMoviesAdapter(AddNewMovieActivity.this, 0, mMoviesList));
        final AlertDialog alertDialog = builder.show();
        listView.setOnItemClickListener(new OnItemClickListener() {
        	@Override
        	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long id) {
        		alertDialog.dismiss();
        		Movie movie = (Movie) listView.getItemAtPosition(position);
        		if(FavouriteMoviesHelper.getInstance().isOnline(AddNewMovieActivity.this)) {
        			if(FavouriteMovieConstants.DBG) Log.d(FavouriteMovieConstants.TAG, "Saving movie");
        			new SaveMovieTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, movie);
        		}
        		//Intent startMainActivityIntent = new Intent(AddNewMovieActivity.this, FavouriteMoviesActivity.class);
        		//startMainActivityIntent.putExtra(FavouriteMovieConstants.MOVIE, movie);
        		//startActivity(startMainActivityIntent);
        		//AddNewMovieActivity.this.finish();
        	}
		});

	}

	private class SearchMovieTask extends AsyncTask<String, String, String> {

		private ProgressDialog pDialog;
		@Override
		protected void onPreExecute() {
		    super.onPreExecute();
		    pDialog = new ProgressDialog(AddNewMovieActivity.this);
		    pDialog.setIndeterminate(true);
		    pDialog.setCancelable(false);
		    pDialog.setMessage("Searching " + mediaItem);
		    pDialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
		    mMoviesList = IMDbHelper.getInstance().getMoviesBasedOnSearch(params[0]);
		    return null;
		}

		@Override
		protected void onPostExecute(String result) {
		    super.onPostExecute(result);
		    if(pDialog != null && pDialog.isShowing()) {
		    	pDialog.dismiss();
		    }
		    showFoundMoviesDialog();
		}
	}
	
	class SaveMovieTask extends AsyncTask<Movie, String, String> {
		@Override
		protected void onPreExecute() {
		    super.onPreExecute();
		    if(FavouriteMovieConstants.DBG) Log.d(FavouriteMovieConstants.TAG, "Save movie - onPreExecute -B");
		    progressDialog = new ProgressDialog(AddNewMovieActivity.this);
		    progressDialog.setMessage(getString(R.string.saving_movie));
		    progressDialog.setCancelable(false);
		    progressDialog.show();
		}

		@Override
		protected String doInBackground(Movie... params) {
			if(FavouriteMovieConstants.DBG) Log.d(FavouriteMovieConstants.TAG, "Saving new movie: " + params[0].toString());
			try {
				Movie movie = params[0];
				String releaseDate = IMDbHelper.getInstance().getReleaseDate(movie.getImdbUrl());
				String rating = IMDbHelper.getInstance().getIMDBRating(movie.getImdbUrl());
				String description = IMDbHelper.getInstance().getIMDBDescription(movie.getImdbUrl());
				String userID = prefs.getString(FavouriteMovieConstants.PREF_USER_ID, "");

				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
				nameValuePairs.add(new BasicNameValuePair(FavouriteMovieConstants.USER_ID, userID));
				nameValuePairs.add(new BasicNameValuePair(FavouriteMovieConstants.TITLE, movie.getTitle()));
				nameValuePairs.add(new BasicNameValuePair(FavouriteMovieConstants.IMDB_URL, movie.getImdbUrl()));
				nameValuePairs.add(new BasicNameValuePair(FavouriteMovieConstants.PHOTO_URL, movie.getPhotoUrl()));
				nameValuePairs.add(new BasicNameValuePair(FavouriteMovieConstants.RELEASE_DATE, releaseDate));
				nameValuePairs.add(new BasicNameValuePair(FavouriteMovieConstants.RATING, rating));
				nameValuePairs.add(new BasicNameValuePair(FavouriteMovieConstants.DESCRIPTION, description));

				Log.d(FavouriteMovieConstants.TAG, "*******************:" + nameValuePairs.toString());
				
				JSONObject json = jsonParser.makeHttpRequest(FavouriteMovieConstants.urlSaveNewMovie, JSONParser.POST, nameValuePairs);
				responseMessage = json.getString(FavouriteMovieConstants.JSON_RESPONSE_MESSAGE);
				if(FavouriteMovieConstants.DBG) Log.d(FavouriteMovieConstants.TAG, responseMessage);
			} catch (Exception e) {
				if(FavouriteMovieConstants.DBG) Log.e(FavouriteMovieConstants.TAG, "JSON Error", e);
			}
		    return null;
		}

		@Override
		protected void onPostExecute(String result) {
		    super.onPostExecute(result);
		    if(progressDialog != null && progressDialog.isShowing()) {
		    	progressDialog.dismiss();
		    }
		    if(responseMessage != null && !responseMessage.isEmpty()) {
		    	showDialog();
		    } else {
		    	AddNewMovieActivity.this.finish();
		    }
		}
	}

	private void showDialog() {
		AlertDialog.Builder alertDialog  = new AlertDialog.Builder(this);

		alertDialog.setTitle(getString(R.string.app_name));
		alertDialog.setMessage(responseMessage);
		alertDialog.setPositiveButton("OK", new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				finish();
			}
		});
		alertDialog.setCancelable(false);
		alertDialog.create().show();
	}
	
}
