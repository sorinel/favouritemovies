package ro.ansoft.favouritemovies;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends Activity {

	private EditText userNameEditText;
	private EditText passwordEditText;
	private Button loginButton;
	private Button cancelButton;
	private JSONParser jsonParser;
	private CheckBox rememberMeCheckBox;
	private SharedPreferences prefs;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_login);
	    userNameEditText = (EditText) findViewById(R.id.login_user_name);
	    passwordEditText = (EditText) findViewById(R.id.login_password);
	    loginButton = (Button) findViewById(R.id.login_button);
	    cancelButton = (Button) findViewById(R.id.cancel_button);
	    rememberMeCheckBox = (CheckBox) findViewById(R.id.remember_me_checkbox);
	    jsonParser = new JSONParser();
	    prefs = getSharedPreferences(FavouriteMovieConstants.PREF_NAME, Context.MODE_PRIVATE);

	    loginButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				checkLogin();
			}
		});
	    
	    cancelButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	    
	    userNameEditText.setText(prefs.getString(FavouriteMovieConstants.PREF_USER_NAME, ""));
	    passwordEditText.setText(prefs.getString(FavouriteMovieConstants.PREF_PASSWORD, ""));
	    if(!prefs.getString(FavouriteMovieConstants.PREF_USER_NAME, "").isEmpty()) {
	    	rememberMeCheckBox.setChecked(true);
	    } else {
	    	rememberMeCheckBox.setChecked(false);
	    }
	}

	@Override
	protected void onStart() {
	    super.onStart();
	}

	@Override
	protected void onResume() {
	    super.onResume();
	}

	@Override
	protected void onStop() {
	    super.onStop();
	}

	@Override
	protected void onDestroy() {
	    super.onDestroy();
	}

	private void checkLogin() {
		String userName = userNameEditText.getText().toString();
		String password = passwordEditText.getText().toString();

		if(userName == null || userName.isEmpty()) {
			Toast.makeText(this, "User name must not be empty", Toast.LENGTH_SHORT).show();
			return;
		}
		if(password == null || password.isEmpty()) {
			Toast.makeText(this, "Password must not be empty", Toast.LENGTH_SHORT).show();
			return;
		}
		if(rememberMeCheckBox.isChecked()) {
			prefs.edit().putString(FavouriteMovieConstants.PREF_USER_NAME, userName).commit();
			prefs.edit().putString(FavouriteMovieConstants.PREF_PASSWORD, password).commit();
		} else {
			prefs.edit().putString(FavouriteMovieConstants.PREF_USER_NAME, "").commit();
			prefs.edit().putString(FavouriteMovieConstants.PREF_PASSWORD, "").commit();
		}
		new LoginTask().execute();
	}

	private class LoginTask extends AsyncTask<String, String, String> {
		private ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
		    super.onPreExecute();
		    progressDialog = new ProgressDialog(LoginActivity.this);
		    progressDialog.setMessage(getString(R.string.loging));
		    progressDialog.setCancelable(false);
		    progressDialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			String userName = userNameEditText.getText().toString();
			String password = passwordEditText.getText().toString();

			try {
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
				nameValuePairs.add(new BasicNameValuePair(FavouriteMovieConstants.JSON_USER_NAME, userName));
				nameValuePairs.add(new BasicNameValuePair(FavouriteMovieConstants.JSON_PASSWORD, password));
				if(FavouriteMovieConstants.DBG) Log.d(FavouriteMovieConstants.TAG, "login http params: " + nameValuePairs.toString());

				JSONObject json = jsonParser.makeHttpRequest(FavouriteMovieConstants.urlLogin, JSONParser.POST, nameValuePairs);
				String responseCode = json.getString(FavouriteMovieConstants.JSON_RESPONSE_CODE);
				String message = json.getString(FavouriteMovieConstants.JSON_RESPONSE_MESSAGE);
				String userId = json.getString(FavouriteMovieConstants.JSON_RESPONSE_USER_ID);

				if(FavouriteMovieConstants.JSON_RESPONSE_OK.equals(responseCode)) {
					prefs.edit().putString(FavouriteMovieConstants.PREF_USER_ID, userId).commit();
					startActivity(new Intent(LoginActivity.this, FavouriteMoviesActivity.class));
				} else {
					showDialog(message);
				}
				Log.d(FavouriteMovieConstants.TAG, json.toString());
			} catch (Exception e) {
				Log.e(FavouriteMovieConstants.TAG, "JSON Error", e);
			}
		    return null;
		}

		@Override
		protected void onPostExecute(String result) {
		    super.onPostExecute(result);
		    if(progressDialog != null && progressDialog.isShowing()) {
		    	progressDialog.dismiss();
		    }
		}
	}

	private void showDialog(final String message) {
		LoginActivity.this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				AlertDialog.Builder alertDialog  = new AlertDialog.Builder(LoginActivity.this);

				alertDialog.setTitle(getString(R.string.app_name));
				alertDialog.setMessage(message);
				alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						
					}
				});
				alertDialog.setCancelable(false);
				alertDialog.create().show();
			}
		});
	}

}
