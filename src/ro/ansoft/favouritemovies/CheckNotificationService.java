package ro.ansoft.favouritemovies;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

public class CheckNotificationService extends Service {
	private Timer mTimer;
	private TimerTask mTimerTask;

	@Override
	public IBinder onBind(Intent intent) {
	    return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		final long HOURS_6 = 21600000;
		try {
			mTimer.schedule(mTimerTask, new Date(), HOURS_6);
		} catch(IllegalStateException e) {
			e.printStackTrace();
		}
		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public void onCreate() {
	    super.onCreate();
	    Log.d(FavouriteMovieConstants.TAG, "CheckNotificationService - oncreate");
	    mTimer = new Timer();
	    mTimerTask = new TimerTask() {
			@Override
			public void run() {
				new Thread(new Runnable() {
					@Override
					public void run() {
						JSONParser jsonParser = new JSONParser();
						List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
						SharedPreferences prefs = getSharedPreferences(FavouriteMovieConstants.PREF_NAME, Context.MODE_PRIVATE);
						String userID = prefs.getString(FavouriteMovieConstants.PREF_USER_ID, "");
						
						nameValuePairs.add(new BasicNameValuePair(FavouriteMovieConstants.USER_ID, userID));

						JSONObject json = jsonParser.makeHttpRequest(FavouriteMovieConstants.urlCheckNotifications, JSONParser.POST, nameValuePairs);

						try {
							if(json.getString(FavouriteMovieConstants.JSON_RESPONSE_CODE).equals(FavouriteMovieConstants.JSON_RESPONSE_OK)) {
								int count = json.getInt(FavouriteMovieConstants.JSON_MOVIE_COUNT);
								String movies = "";
								for(int i = 0; i < count; i++) {
									if(i >= 1) {
										String currentMovie = json.getString(FavouriteMovieConstants.MOVIE + i);
										movies += ", " + currentMovie;
									} else {
										movies = json.getString(FavouriteMovieConstants.MOVIE + i);
									}
								}
								sendNotification(movies);
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}).start();
			}
		};
	}
	
	private void sendNotification(String movies) {
		Log.d(FavouriteMovieConstants.TAG, "CheckNotificationService - sending notification");
		NotificationCompat.Builder builder =
	            new NotificationCompat.Builder(this)
	                    .setSmallIcon(R.drawable.logo)
	                    .setContentTitle("Favourite Movies")
	                    .setContentText("The following movies are due to air soon: " + movies);
	    int NOTIFICATION_ID = 12345;

	    Intent targetIntent = new Intent(this, FavouriteMoviesActivity.class);
	    PendingIntent contentIntent = PendingIntent.getActivity(this, 0, targetIntent, PendingIntent.FLAG_UPDATE_CURRENT);
	    builder.setContentIntent(contentIntent);
	    builder.setAutoCancel(true);

	    NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
	    manager.notify(NOTIFICATION_ID, builder.build());
	}

	@Override
	public void onDestroy() {
	    super.onDestroy();
	    mTimer.cancel();
	}

}
