package ro.ansoft.favouritemovies;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class ViewMovieDetailsFragment extends DialogFragment {

	private TextView titleTextView;
	private ImageView image;
	private TextView ratingTextView;
	private TextView descriptionTextView;
	private ListView userReviewsList;
	private ReviewsAdapter reviewsAdapter;
	private Movie movie;
	private ArrayList<MovieReview> reviewsLists;

	public ViewMovieDetailsFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_movie_details, container);
		titleTextView = (TextView) view.findViewById(R.id.title_detail);
		ratingTextView = (TextView) view.findViewById(R.id.imdb_rating_detail);
		descriptionTextView = (TextView) view.findViewById(R.id.imdb_description_detail);
		image =(ImageView) view.findViewById(R.id.image_detail);
		userReviewsList = (ListView) view.findViewById(R.id.user_review_detail);
		reviewsLists = new ArrayList<MovieReview>();
		reviewsAdapter = new ReviewsAdapter(getActivity(), -1, reviewsLists);
		userReviewsList.setAdapter(reviewsAdapter);
		userReviewsList.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				v.getParent().requestDisallowInterceptTouchEvent(true);
				return false;
			}
		});
		movie = (Movie) getArguments().getParcelable(FavouriteMovieConstants.MOVIE);

		getDialog().setTitle("Movie Details");

		titleTextView.setText(movie.getTitle());
		ratingTextView.setText("IMDb rating: " + movie.getRating());
		descriptionTextView.setText(movie.getDescription());		

		new Thread(new Runnable() {
			@Override
			public void run() {
				URL newurl;
		        try {
			        newurl = new URL(movie.getPhotoUrl());
			        final Bitmap bitmap = BitmapFactory.decodeStream(newurl.openConnection() .getInputStream());
			        ViewMovieDetailsFragment.this.getActivity().runOnUiThread(new Runnable() {
						@Override
						public void run() {
							image.setImageBitmap(bitmap);
						}
					});
		        } catch (MalformedURLException e) {
			        e.printStackTrace();
		        } catch (IOException e) {
			        e.printStackTrace();
		        }
			}
		}).start();
		
		if(FavouriteMoviesHelper.getInstance().isOnline(getActivity())) {
			new LoadMovieReviewsTask().execute();
		}

		return view;
	}

	private class LoadMovieReviewsTask extends AsyncTask<String, String, String> {
		private ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
		    super.onPreExecute();
		    progressDialog = new ProgressDialog(getActivity());
		    progressDialog.setMessage(getString(R.string.loading));
		    progressDialog.setCancelable(false);
		    progressDialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			try {
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
				nameValuePairs.add(new BasicNameValuePair(FavouriteMovieConstants.MOVIE_ID, movie.getId()));
				Log.d(FavouriteMovieConstants.TAG, "***********" +  movie.getId());
				
				JSONParser jsonParser = new JSONParser();
				JSONObject json = jsonParser.makeHttpRequest(FavouriteMovieConstants.urlGetMovieReviews, JSONParser.POST, nameValuePairs);
				saveReviews(json);
				Log.d(FavouriteMovieConstants.TAG, json.toString());
			} catch (Exception e) {
				Log.e(FavouriteMovieConstants.TAG, "JSON Error", e);
			}
		    return null;
		}

		private void saveReviews(JSONObject json) throws JSONException {
			reviewsLists.clear();
			JSONArray jsonReviewers = json.getJSONArray("names");
			JSONArray jsonReviews = json.getJSONArray("reviews");
			JSONArray jsonRatings = json.getJSONArray("ratings");

			for(int i = 0; i < jsonReviewers.length(); i++) {
				String reviewer = jsonReviewers.getString(i);
				String review = jsonReviews.getString(i);
				String rating = jsonRatings.getString(i);

				MovieReview movieReview = new MovieReview();
				movieReview.setReviewer(reviewer);
				movieReview.setRating(rating);
				movieReview.setReview(review);

				reviewsLists.add(movieReview);
			}
			ViewMovieDetailsFragment.this.getActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					reviewsAdapter.notifyDataSetChanged();
				}
			});
		}
		
		@Override
		protected void onPostExecute(String result) {
		    super.onPostExecute(result);
		    if(progressDialog != null && progressDialog.isShowing()) {
		    	progressDialog.dismiss();
		    }
		}
	}

	static class MovieReview {
		private String reviewer;
		private String review;
		private String rating;

		public String getReviewer() {
			return reviewer;
		}
		public void setReviewer(String reviewer) {
			this.reviewer = reviewer;
		}
		public String getReview() {
			return review;
		}
		public void setReview(String review) {
			this.review = review;
		}
		public String getRating() {
			return rating;
		}
		public void setRating(String rating) {
			this.rating = rating;
		}
	}

}
