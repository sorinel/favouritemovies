package ro.ansoft.favouritemovies;

public class FavouriteMovieConstants {

	static final String TAG = "FavouriteMoviesActivity";
	static final boolean DBG = true;
	static final String urlSaveNewMovie = "http://ansoft.ro/new/fav_movies/save_movie.php";
	static final String urlGetMoviesList = "http://ansoft.ro/new/fav_movies/get_movies_list.php";
	static final String urlDeleteMovieList = "http://ansoft.ro/new/fav_movies/delete_movies.php";
	static final String urlCreateAccount = "http://ansoft.ro/new/fav_movies/create_account.php";
	static final String urlLogin = "http://ansoft.ro/new/fav_movies/login.php";
	static final String urlResetPassword = "http://ansoft.ro/new/fav_movies/reset_password.php";
	static final String urlAddReview = "http://ansoft.ro/new/fav_movies/add_review.php";
	static final String urlGetMovieReviews = "http://ansoft.ro/new/fav_movies/get_movie_reviews.php";
	static final String urlCheckNotifications = "http://ansoft.ro/new/fav_movies/check_notifications.php";
	static final String SELECTED_TAB = "SelectedTab";
	static final String MOVIE = "MOVIE";

	//PREFS
	static final String PREF_NAME = "FavouriteMovies";
	static final String PREF_USER_NAME = "user_name";
	static final String PREF_USER_ID = "user_id";
	static final String PREF_PASSWORD = "password";

	static final String MOVIE_ID = "movie_id";
	static final String USER_ID = "user_id";
	static final String TITLE = "title";
	static final String REVIEW = "review";
	static final String RATING = "rating";
	static final String CATEGORY = "category";
	static final String RELEASE_DATE = "releaseDate";
	static final String DESCRIPTION = "description";
	static final String PHOTO_URL = "photoUrl";
	static final String IMDB_URL = "imdbUrl";
	static final String JSON_RESPONSE_CODE = "code";
	static final String JSON_MOVIE_COUNT = "movie_count";
	static final String JSON_RESPONSE_MESSAGE = "message";
	static final String JSON_RESPONSE_USER_ID = "userId";
	static final String JSON_RESPONSE_OK = "0";

	// create account
	static final String JSON_USER_NAME = "c1";
	static final String JSON_EMAIL = "c2";
	static final String JSON_PASSWORD = "c3";
}
