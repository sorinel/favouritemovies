package ro.ansoft.favouritemovies;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ReviewsAdapter  extends ArrayAdapter<ViewMovieDetailsFragment.MovieReview> {

	private Context mContext;
	private ArrayList<ViewMovieDetailsFragment.MovieReview> mMovieReviews;
	private ViewHolder viewHolder;

	public ReviewsAdapter(Context context, int resource, ArrayList<ViewMovieDetailsFragment.MovieReview> reviews) {
	    super(context, resource, reviews);
	    mContext = context;
	    mMovieReviews = reviews;
    }

	@Override
	public int getCount() {
	    return mMovieReviews.size();
	}

	@Override
	public ViewMovieDetailsFragment.MovieReview getItem(int position) {
	    return mMovieReviews.get(position);
	}

	@Override
	public int getPosition(ViewMovieDetailsFragment.MovieReview item) {
	    return mMovieReviews.indexOf(item);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {		
	     if(convertView == null) {
	        LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
	        convertView = inflater.inflate(R.layout.review_row, parent, false);

	        viewHolder = new ViewHolder();
	        viewHolder.rating = (TextView) convertView.findViewById(R.id.review_rating);
	        viewHolder.review = (TextView) convertView.findViewById(R.id.review_review);
	        viewHolder.reviewer = (TextView) convertView.findViewById(R.id.review_reviewer);

	        convertView.setTag(viewHolder);
	    } else {
	        viewHolder = (ViewHolder) convertView.getTag();
	    }

	    // object item based on the position
	    ViewMovieDetailsFragment.MovieReview review = mMovieReviews.get(position);

	    // assign values if the object is not null
	    if(review != null) {
	    	viewHolder.rating.setText("Rating: " + review.getRating());
	    	viewHolder.review.setText(review.getReview());
	    	viewHolder.reviewer.setText(review.getReviewer());
	    }
	    return convertView;
	}

	static class ViewHolder {
		TextView reviewer;
		TextView rating;
		TextView review;
	}

}
