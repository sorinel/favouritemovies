package ro.ansoft.favouritemovies;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class StartActivity extends Activity {

	private Button loginButton;
	private Button createAccountButton;
	private Button changePasswordButton;
	private Button forgotPasswordButton;
	private JSONParser jsonParser;
	private String userName;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_start);

	    loginButton = (Button) findViewById(R.id.start_login_button);
	    createAccountButton = (Button) findViewById(R.id.start_create_account_button);
	    changePasswordButton = (Button) findViewById(R.id.start_change_password_button);
	    forgotPasswordButton = (Button) findViewById(R.id.start_forgot_password_button);
	    jsonParser = new JSONParser();

	    loginButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(StartActivity.this, LoginActivity.class));
				finish();
			}
		});
	    
	    createAccountButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(StartActivity.this, CreateAccountActivity.class));
			}
		});
	    
	    forgotPasswordButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				userName = getSharedPreferences(FavouriteMovieConstants.PREF_NAME, Context.MODE_PRIVATE).getString(FavouriteMovieConstants.PREF_USER_NAME, "");
				if(userName.isEmpty()) {
					getUserNameInput();
				} else {
					if(FavouriteMoviesHelper.getInstance().isOnline(StartActivity.this)) {
						new ResetPasswordTask().execute(userName);
					}
				}
			}
		});
	    startService(new Intent(this, CheckNotificationService.class));
	}

	@Override
	protected void onStart() {
	    super.onStart();
	}

	@Override
	protected void onStop() {
	    super.onStop();
	}

	@Override
	protected void onDestroy() {
	    super.onDestroy();
	}

	private class ResetPasswordTask extends AsyncTask<String, String, String> {
		private ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
		    super.onPreExecute();
		    progressDialog = new ProgressDialog(StartActivity.this);
		    progressDialog.setMessage(getString(R.string.resetting_password));
		    progressDialog.setCancelable(false);
		    progressDialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			String userName = params[0];

			try {
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
				nameValuePairs.add(new BasicNameValuePair(FavouriteMovieConstants.JSON_USER_NAME, userName));
				if(FavouriteMovieConstants.DBG) Log.d(FavouriteMovieConstants.TAG, "reset password http params: " + nameValuePairs.toString());

				JSONObject json = jsonParser.makeHttpRequest(FavouriteMovieConstants.urlResetPassword, JSONParser.POST, nameValuePairs);
				String responseCode = json.getString(FavouriteMovieConstants.JSON_RESPONSE_CODE);
				String message = json.getString(FavouriteMovieConstants.JSON_RESPONSE_MESSAGE);

				if(FavouriteMovieConstants.JSON_RESPONSE_OK.equals(responseCode)) {
					showDialog("Your new password was emailed to you.");
				} else {
					showDialog(message);
				}
				Log.d(FavouriteMovieConstants.TAG, json.toString());
			} catch (Exception e) {
				Log.e(FavouriteMovieConstants.TAG, "JSON Error", e);
			}
		    return null;
		}

		@Override
		protected void onPostExecute(String result) {
		    super.onPostExecute(result);
		    if(progressDialog != null && progressDialog.isShowing()) {
		    	progressDialog.dismiss();
		    }
		}
	}

	private void showDialog(final String message) {
		StartActivity.this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				AlertDialog.Builder alertDialog  = new AlertDialog.Builder(StartActivity.this);

				alertDialog.setTitle(getString(R.string.app_name));
				alertDialog.setMessage(message);
				alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						
					}
				});
				alertDialog.setCancelable(false);
				alertDialog.create().show();
			}
		});
	}
	
	void getUserNameInput () {
		AlertDialog.Builder alert = new AlertDialog.Builder(this);

		alert.setTitle("Reset password");
		alert.setMessage("Enter user name");

		final EditText input = new EditText(this);
		alert.setView(input);

		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				userName = input.getText().toString();
				if(FavouriteMoviesHelper.getInstance().isOnline(StartActivity.this)) {
					new ResetPasswordTask().execute(userName);
				}
			}
		});

		alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
			}
		});

		alert.show();
	}
	
}
